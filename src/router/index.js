import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import Sales from "../views/Sales";
import Customers from "../views/Customers";
import Products from "../views/Products";
import Stocks from "../views/Stocks";
import SalesReps from "../views/SalesReps";
import RoutePlan from "../views/RoutePlan";
import Suppliers from "../views/Suppliers";
import Purchases from "../views/Purchases";
import Reports from "../views/Reports";
import List from "../views/List";
import Questionnaires from "../views/Questionnaires";
import Orders from "../views/Orders";
import Outlets from "../views/Outlets";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'dashboard',
        component: Dashboard,
    },
    {
        path: '/sales',
        name: 'sales',
        component: Sales,
    },
    {
        path: '/customers',
        name: 'customers',
        component: Customers
    },
    {
        path: '/products',
        name: 'products',
        component: Products
    },
    {
        path: '/stocks',
        name: 'stocks',
        component: Stocks
    },
    {
        path: '/visits/list',
        name: 'list',
        component: List
    },
    {
        path: '/visits/questionnaires',
        name: 'questionnaires',
        component: Questionnaires
    },
    {
        path: '/sales-reps',
        name: 'sales-reps',
        component: SalesReps
    },
    {
        path: '/route-plan',
        name: 'route-plan',
        component: RoutePlan
    },
    {
        path: '/stock-lists/list',
        name: 'list',
        component: List
    },
    {
        path: '/stock-lists/orders',
        name: 'orders',
        component: Orders
    },
    {
        path: '/stock-lists/outlets',
        name: 'outlets',
        component: Outlets
    },
    {
        path: '/suppliers',
        name: 'suppliers',
        component: Suppliers
    },
    {
        path: '/purchases',
        name: 'purchases',
        component: Purchases
    },
    {
        path: '/reports',
        name: 'reports',
        component: Reports
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router
